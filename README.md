# EKKI

## Resume
`Ekki: Online money transfer made easy`

### Dependencies
* **Node.js** 10.12.0
* **Npm** 6.0.9
* **React** 16.8
* **Docker** 18.09.6
* **Docker Compose** 1.23.2
* **express-generator**

### Install & Start Client
* cd ekki-client
* npm install
* set an **.env** file (on the root folder ekki-client): 
  * REACT_APP_API_URL=http://localhost:3000
  * REACT_APP_LOGGED_USER=12345678910
* npm start
  
### Install & Start Server
* cd ekki-server
* npm install
* set an **.env** file (on the root folder ekki-server): 
  * DB_HOST=localhost
  * DB_USER=lucas
  * DB_PASS=123456
  * DB_NAME=ekkidb
* npm start
* **Server** will be available at: [localhost](http://localhost:3000)

### Install & Start Database
* cd ekki-database
* docker-compose up app (installs and starts **MYSQL** + **phpMyAdmin**)
* **phpMyAdmin** will be available at: [localhost](http://localhost:80)