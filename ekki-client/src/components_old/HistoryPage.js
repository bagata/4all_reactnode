/**
 * Displays all money transfers
 *
 * @summary History page
 * @author Lucas Bagatini
 *
 */

import React, { Component } from 'react';
import AuthenticateUser, { AuthContext } from './Auth';
import TransferTable from './TransferTable';

class HistoryPage extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading: true
    }
  }

  render() {
    return (
      <div className="container margin-top-20">
        <AuthContext.Consumer>
          {user =>
            !!user ? (
              <TransferTable user={user} />
            ) : (
              <div>No transfers were made</div>
            )
          }
        </AuthContext.Consumer>
      </div>
    );
  }
}

export default AuthenticateUser(HistoryPage);
