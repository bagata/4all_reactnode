/**
 * Handles money transference
 *
 * @summary Transfer page
 * @author Lucas Bagatini
 *
 */

import React, { Component } from 'react';
import AuthenticateUser, { AuthContext } from './Auth';
import Contact from './Contact';

class TransferPage extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading: true
    }
  }

  render() {
    return (
      <div className="container margin-top-20">
        <AuthContext.Consumer>
          {user =>
            !!user ? (
              <Contact user={user} />
            ) : (
              <div>No contacts</div>
            )
          }
        </AuthContext.Consumer>
      </div>
    );
  }
}

export default AuthenticateUser(TransferPage);
