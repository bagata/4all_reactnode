import React, { Component } from 'react';

class Contact extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      contacts: [],
      amount: '',
      contact: '',
    }
  }

  componentDidMount = () => {
    this.getContacts(this.props.user.id);
  }

  getContacts = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}contacts/${id}/info`)
    .then((response) => { 
      if(!response.ok) {
        throw Error(response.statusText);
      }
      return response.json()
    })
    .then((responseJson) => {
      if(!!responseJson && responseJson.success && responseJson.data) {
        this.setState({
          isLoading: false,
          contacts: responseJson.data,
        })
      }
    })
    .catch((error) => {
      this.setState({
        isLoading: false
      });
      console.error(error);
    });
  }

  transferMoney = () => {
    const amount = this.state.amount;
    const transfer =
    {
      "transfer": {
        "account_id": this.props.user.account_id,
        "recipient_id": this.state.contact,
        "amount":  amount
      }
    };

    fetch(`${process.env.REACT_APP_API_URL}transfers`, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(transfer)
    })
    .then((response) => { 
      // if(!response.ok) {
      //   throw Error(response.statusText);
      // }
      // return response.json()
    })
    .then((responseJson) => {
      // if(!!responseJson && responseJson.success && responseJson.data) {
      //   this.setState({
      //     isLoading: false,
      //     contacts: responseJson.data,
      //   })
      // }
    })
    .catch((error) => {
      this.setState({
        isLoading: false
      });
      console.error(error);
    });
  }

  renderContacts = () => {
    const contacts = this.state.contacts;
    return (
      contacts.map((contact, index) => {
        return (
          <div className="row margin-top-20" key={index}>
            <div className="column" onClick={() => this.setState({contact: contact.recipient_id})}>
              <div className={"contact" + (this.state.contact === contact.recipient_id ? " contact--selected" : "")}>
                <div>{contact.name}</div>
                <em>{contact.cpf}</em>
              </div>
            </div>
          </div>
        )
      })
    );
  }

  render() {
    const userData = this.props.user;
    return (
      <div className="container margin-top-20">
        <div className="row">
          <div className="column">
            <p>
              {userData.name}<br/>
              <em className={userData.balance > 0 ? "positive-balance" : "negative-balance"}>{userData.balance}</em><br/>
              <em className={userData.credit_limit > 0 ? "positive-balance" : "negative-balance"}>{userData.credit_limit}</em>
            </p>
          </div>
        </div>
        {/* Show form if there is at least one contact */}
        { this.state.contacts.length > 0  ? 
          (
          <div className="row">
              <div className="column">
                <h1>Transference</h1>
                <form className="transfer-form">
                  <fieldset>
                    <label htmlFor="nameField">Amount</label>
                    <input type="text" placeholder="Money to transfer" onChange={(event) => this.setState({amount: event.target.value})} value={this.state.amount}/>
                  </fieldset>
                  {this.renderContacts()}
                  <button className="button-alt button-block margin-top-20" type="button" onClick={() => this.transferMoney()} >SEND MONEY</button>
                </form>
              </div>
            </div>
          ) 
          : null 
        }
      </div>
    );
  }
}

export default Contact;
