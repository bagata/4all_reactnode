/**
 *  Route not found
 *
 * @summary 404 page
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { NavLink } from 'react-router-dom';

const NotFound = () => {
    return (
      <div className="container text-center">
        <strong>Ops! 404</strong>
        <br/>
        <NavLink className="button-alt margin-top-20" to='/'>
          <span className="text">Take me Home</span>
        </NavLink>
      </div>
    );
}

export default NotFound;
