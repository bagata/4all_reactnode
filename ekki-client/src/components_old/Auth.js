import React, { Component } from 'react';
export const AuthContext = React.createContext(null);
 
const AuthenticateUser = (ComposedComponent) => {
  class Authenticate extends Component {
    state = {
      user: null
    };
 
    componentDidMount = async () => {
      try {
        const userDataStr = localStorage.getItem('ekki_user');
        if(!!userDataStr) {
          this.setState({ user: JSON.parse(userDataStr) });
        }
        else {
          const response = await this.getUserData();
          if (response.status === 200) {
            const json = await response.json();
            if(json.success) {
              localStorage.setItem('ekki_user', JSON.stringify(json.data));
              this.setState({ user: json.data });
            }
          }
        }
      }
      catch (error) {
        console.error(error)
      }
    }

    getUserData = async () => {
      return fetch(`${process.env.REACT_APP_API_URL}users/cpf/${process.env.REACT_APP_LOGGED_USER}`);
    }
 
    render() {
      const { user } = this.state;
      return(
        <AuthContext.Provider value={user}>
          <ComposedComponent />
        </AuthContext.Provider>
      );
    }
  }
  return Authenticate;
};
 
export default AuthenticateUser;