import React, { Component } from 'react';

class TransferTable extends Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      transfers: []
    }
  }

  componentDidMount = () => {
    this.getTransferHistory(this.props.user.account_id);
  }

  getTransferHistory = (accountId) => {
    fetch(`${process.env.REACT_APP_API_URL}transfers/account/${accountId}`)
    .then((response) => { 
      if(!response.ok) {
        throw Error(response.statusText);
      }
      return response.json()
    })
    .then((responseJson) => {
      if(!!responseJson && responseJson.success && responseJson.data) {
        this.setState({
          isLoading: false,
          transfers: responseJson.data,
        })
      }
    })
    .catch((error) => {
      this.setState({
        isLoading: false
      });
      console.error(error);
    });
  }

  renderTransferHistory = () => {
    const transfers = this.state.transfers;
    return (
      transfers.map((transfer, index) => {
        return (
          <div className="margin-top-20" key={index}>
            <div>{transfer.name}</div>
            <div>{transfer.cpf}</div>
            <div>{transfer.timestamp}</div>
          </div>
        )

      })
    );
  }

  render() {
    return (
      <div className="container margin-top-20">
        { this.state.transfers.length > 0  ? this.renderTransferHistory() : null }
      </div>
    );
  }
}

export default TransferTable;
