/**
 * Routes structure and layout/page components
 *
 * @summary Routes and layout
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import './scss/index.scss';
import App from './App';
import Navigation from './components/Navigation';
import Wallet from './components/Wallet';
import HistoryPage from './components/HistoryPage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import * as serviceWorker from './serviceWorker';

// Routes
const routing = (
    <Router>
        <App>
            <Wallet/>
            <Navigation/>
            <Switch>
                <Route path="/" exact component={HistoryPage} />
            </Switch> 
        </App>
    </Router>
);

ReactDOM.render(routing, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();