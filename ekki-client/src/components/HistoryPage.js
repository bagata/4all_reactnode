/**
 * Displays history of transfers
 *
 * @summary History Page
 * @author Lucas Bagatini
 *
 */

import React, { Component } from 'react';
import History from './History';

class HistoryPage extends Component {
  state = {
    transfersData: []
  };

  componentDidMount = async () => {
    try {
      // Checks local storage for user data
      const userDataStr = localStorage.getItem('ekki_user');
      if(!!userDataStr) {

        // Loads data from local storage
        const userDataParsed = JSON.parse(userDataStr);

        // Makes a request to the API to get transfer history data
        const response = await this.getByAccountId(userDataParsed.accountId);
        if (response.status === 200) {
          const json = await response.json();
          if(json.success) {
            const transfersData = json.data;
            this.setState({ transfersData: transfersData });
          }
        }
      }
    }
    catch (error) {
      console.error(error)
    }
  }

  /**
   * Calls API to get transfer history 
   */
  getByAccountId = async (id) => {
    return fetch(`${process.env.REACT_APP_API_URL}transfers/account/${id}`);
  }

  /**
   * Passes transfer data to History component
   */
  render = () => {
    return <History transferHistory={this.state.transfersData}/>
  }
}
export default HistoryPage;
