/**
 * Layout for profile and account data
 *
 * @summary Profile and account data
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { Box, Text } from 'grommet';
import { Article, Transaction } from 'grommet-icons';

const Navigation = withRouter(({ location }) => {
  const pathname = location.pathname;
  const active = 'accent-1';
  const inactive = 'dark-6';
  return (
    <Box
      background='dark-1'
      direction='row'
      align='center'
      justify='evenly'
      pad={{ horizontal: 'large', vertical: 'small' }}
      gap='medium'
      reponsive='true'
      a11yTitle='Tabs to navigate between screens'
    >
      <NavLink className="no-text-decoration" to='/' activeStyle={{ color: "gray" }}>
        <Box pad='medium' align='center' round='small' gap='small'>
          <Article size='medium' color={!!pathname && pathname === '/' ? active : inactive} />
          <Text size='small' color={!!pathname && pathname === '/' ? active : inactive}>History</Text>
        </Box>
      </NavLink>
      <NavLink className="no-text-decoration" to='/transfer'>
        <Box pad='medium' align='center' round='small' gap='small'>
          <Transaction size='medium' color={!!pathname && pathname === '/transfer' ? active : inactive} />
          <Text size='small' color={!!pathname && pathname === '/transfer' ? active : inactive}>Transfer</Text>
        </Box>
      </NavLink>
    </Box>
  )})
export default Navigation;
