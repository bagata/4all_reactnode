/**
 * Layout for profile and account data
 *
 * @summary Profile and account data
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { Box, Heading, Text } from 'grommet';
import { CreditCard, Currency } from 'grommet-icons';

const Profile = (props) => {
  const user = props.user;
  const account = props.account;

  // Checks if user data is available
  if(!!user && !!account) {
    return (
      <Box
        direction='column'
        pad={{ horizontal: 'large', top: 'large', bottom: 'xsmall' }}
        background={!!account.balance && account.balance >= 0.0 ? 'accent-1' : 'status-critical'}
        align='start'
        a11yTitle='Account balance'
      >
        <Text>{!!user.name ? user.name : ''}</Text>
        <Box
          direction='row'
          gap='xsmall'
        >
          <Heading level={1} size='medium'>Wallet</Heading>
        </Box>
        <Box
          direction='row'
          gap='small'
        >
          <Currency size='medium' />
          <Text>$ {!!account.balance ? account.balance : ''}</Text>
        </Box>
        <Box
          direction='row'
          gap='small'
          pad={{ vertical: 'medium' }}
        >
          <CreditCard size='medium' />
          <Text>$ {!!account.creditLimit ? account.creditLimit : ''}</Text>
        </Box>
      </Box>
    )
  }
  else {
    // Loading
    return null;
  }
}
export default Profile;
