/**
 * Displays current balance and credit limit
 *
 * @summary User Wallet
 * @author Lucas Bagatini
 *
 */

import React, { Component } from 'react';
import Profile from './Profile';

class Wallet extends Component {
  state = {
    userData: null,
    accountData: null
  };

  componentDidMount = async () => {
    try {
      // Checks local storage for user data
      const userDataStr = localStorage.getItem('ekki_user');
      if(!!userDataStr) {

        // Loads data from local storage
        const userDataParsed = JSON.parse(userDataStr);
        this.setState({ userData:  userDataParsed});

        // Makes a request to the API to get account data
        const response = await this.getAccountData(userDataParsed.accountId);
        if (response.status === 200) {
          const json = await response.json();
          if(json.success) {
            const accountData = json.data;
            const accountDataState = {
              'balance': accountData.balance,
              'creditLimit': accountData.credit_limit
            };
            // Stores values in the local storage and state
            this.setState({ accountData: accountDataState });
          }
        }
      }
      else {
        // Makes a request to the API to get user data
        const response = await this.getUserData();
        if (response.status === 200) {
          const json = await response.json();
          if(json.success) {
            const userData = json.data;
            const userLocalStorage = {
              'id': userData.id,
              'accountId': userData.account_id,
              'name': userData.name
            };
            const accountData = {
              'balance': userData.balance,
              'creditLimit': userData.credit_limit
            };
            // Stores values in the local storage and state
            localStorage.setItem('ekki_user', JSON.stringify(userLocalStorage));
            this.setState({ userData: userLocalStorage, accountData: accountData });
          }
        }
      }
    }
    catch (error) {
      console.error(error)
    }
  }

  /**
   * Calls API to get user data 
   */
  getUserData = async () => {
    return fetch(`${process.env.REACT_APP_API_URL}users/cpf/${process.env.REACT_APP_LOGGED_USER}`);
  }

  getAccountData = async (id) => {
    return fetch(`${process.env.REACT_APP_API_URL}accounts/${id}`);
  }

  /**
   * Passes user data to Profile component
   */
  render = () => {
    return <Profile user={this.state.userData} account={this.state.accountData}/>
  }
}
export default Wallet;
