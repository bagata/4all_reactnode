import React from 'react';
import { Box, DataTable, Heading, InfiniteScroll, Text } from "grommet";
import { Add, Subtract } from 'grommet-icons';

const History = (props) => {
  const transferHistory = props.transferHistory;

  const amountFormatter = new Intl.NumberFormat("pt-BR", {
    style: "currency",
    currency: "BRL",
    minimumFractionDigits: 2
  });
  
  const columns = [
    {
      property: "name",
      primary: true,
    },
    {
      property: "timestamp",
      align: "end"
    },
    {
      property: "amount",
      render: datum => amountFormatter.format(datum.amount),
      align: "end",
    }
  ];

  // Checks if transfer history data is available
  if(!!transferHistory) {
    return (
      <Box
        direction='column'
        pad={{ horizontal: 'xsmall', top: 'xsmall' }}
        a11yTitle='Transfer history'
        justify="evenly"
      >
        <InfiniteScroll items={transferHistory}>
          {item => (
            <Box direction='row' key={item.id} pad='medium'>
              <Box direction='column' pad='medium' justify='center' align='center'>
                <Text size='small'>{item.name}</Text>
                <Text size='xsmall'>{item.timestamp}</Text>
              </Box>
              <Box direction='column' pad='medium' justify='center' align='center'>
                <Text size='small'>{amountFormatter.format(item.amount)}</Text>
              </Box>
              <Box direction='column' pad='medium' justify='center' align='center'>
                <Subtract color='status-critical' size='medium'/>
                <Add color='status-ok' size='medium'/>
              </Box>
            </Box>
          )}
        </InfiniteScroll>
      </Box>
    )
  }
  else {
    // Loading
    return null;
  }
}
export default History;
