/**
 * Core of the app: wraps the main content
 *
 * @summary Main App file
 * @author Lucas Bagatini
 *
 */

import React from 'react';
import { Grommet } from 'grommet';

const theme = {
  global: {
      colors: {
      },
      font: {
          family: 'Roboto',
      },
  },
};

const App = (props) => {
  return (
    <Grommet theme={theme} full>
      {props.children}
    </Grommet>
  )
}
export default App;
