/**
 * This is the express server and its configurations
 *
 * @summary express server
 * @author Lucas Bagatini
 * 
 */

import createError from 'http-errors';
import express from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import usersRouter from './routes/users';
import contactsRouter from './routes/contacts';
import accountsRouter from './routes/accounts';
import transfersRouter from './routes/transfers';
import listEndpoints from 'express-list-endpoints';

const API_VERSION = '/api/v1';

const app = express();
const router = express.Router();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


// Routes
app.use(`${API_VERSION}/`, router);
app.use(`${API_VERSION}/users`, usersRouter);
app.use(`${API_VERSION}/contacts`, contactsRouter);
app.use(`${API_VERSION}/accounts`, accountsRouter);
app.use(`${API_VERSION}/transfers`, transfersRouter);

router.get('/', function(req, res, next) {
  const endPoints = listEndpoints(app);
  res.render('index', { title: 'Ekki API v1', endPoints: endPoints });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
