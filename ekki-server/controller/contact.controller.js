/**
 * Contact controller - Has all methods needed to create, read, update or delete a contact in the database
 * Relationship between two users
 * 
 * @summary Contact CRUD
 * @author Lucas Bagatini
 */
import pool from '../middleware/db';
import * as statusHelper from '../helper/status.helper';

/**
 * CREATE Methods
 */

// Creates a new contact
export const createQuery = async (contact) => pool.query('INSERT INTO contacts SET ?', [contact]);
export const create = async (req, res) => {
    const { contact } = req.body;
    try {
        const [result] = await createQuery(contact);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

/**
 * READ Methods
 */

// Gets all contacts
export const listQuery = async () => pool.query('SELECT * FROM contacts');
export const list = async (req, res) => {
    try {
        const [result] = await listQuery();
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets user contacts by user id
export const getByUserIdQuery = async (id) => pool.query('SELECT * FROM contacts WHERE user_id = ?', [id]);
export const getByUserId = async (req, res) => {
    try {
        const { id } = req.params;
        const [result] = await getByUserIdQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Includes contact info (from user table)
export const getFullDataQuery = async (id) => { 
    return pool.query(
        `SELECT ctt.*, usr.*, acc.id AS recipient_id FROM contacts AS ctt
        LEFT JOIN users AS usr
        ON ctt.contact_id = usr.id
        LEFT JOIN accounts AS acc
        ON acc.user_id = usr.id
        WHERE ctt.user_id = ?`,
        [id]
    )
};
export const getFullData = async (req, res) => {
    try {
        const { id } = req.params;
        const [result] = await getFullDataQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

/**
 * UPDATE Methods
 */
export const editQuery = async (id, contact) => pool.query('UPDATE contacts SET ? WHERE user_id = ?', [contact, id]);
export const edit = async (req, res) => {
    const { id } = req.params;
    const { contact } = req.body;
    try {
        const [result] = await editQuery(id, contact);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

 /**
 * DELETE Methods
 */
export const destroyQuery = async (user_id, contact_id) => pool.query('DELETE FROM contacts WHERE user_id = ? AND contact_id = ?', [user_id, contact_id]);
export const destroy = async (req, res) => {
    const { user_id } = req.params;
    const { contact_id } = req.params;
    try {
        const [result] = await destroyQuery(user_id, contact_id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};