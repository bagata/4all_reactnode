/**
 * Account controller - Has all methods needed to create, read, update or delete an account in the database
 * Account stores user credit balance and limit
 *
 * @summary Account CRUD
 * @author Lucas Bagatini
 */
import pool from '../middleware/db';
import * as statusHelper from '../helper/status.helper';

/**
 * CREATE Methods
 */
export const createQuery = async (account) => pool.query('INSERT INTO accounts SET ?', [account]);
export const create = async (req, res) => {
    const { account } = req.body;
    try {
        const [result] = await createQuery(account);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

/**
 * READ Methods
 */

// Gets all accounts
export const listQuery = async () => pool.query('SELECT * FROM accounts');
export const list = async (req, res) => {
    try {
        const [result] = await listQuery();
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets account by id
export const getByIdQuery = async (id) => pool.query('SELECT * FROM accounts WHERE id = ?', [id]);
export const getById = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await getByIdQuery(id);
        res.status(200).send({
            success: true,
            data: result[0]
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets account balance
export const getBalanceQuery = async (id, conn) => conn.query('SELECT balance, credit_limit FROM accounts WHERE id = ?', [id]);

/**
 * UPDATE Methods
 */
export const editQuery = async (id, account) => pool.query('UPDATE accounts SET ? WHERE id = ?', [account, id]);
export const edit = async (req, res) => {
    const { id } = req.params;
    const { account } = req.body;
    try {
        const [result] = await editQuery(id, account);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Transfers money form one account to another 
export const transferBalance = async (accountId, account, recipientId, amount, connection) => {
    try {
        await connection.query('UPDATE accounts SET ? WHERE id = ?', [account, accountId]);
        await connection.query('UPDATE accounts SET balance = (@cur_value := balance) + ? WHERE id = ?', [amount, recipientId]);
    }
    catch (error) {
        console.error(error);
        // await connection.rollback();
        statusHelper.internalServerError(res);
    }
};

 /**
 * DELETE Methods
 */
export const destroyQuery = async (id) => pool.query('DELETE FROM accounts WHERE id = ?', [id]);
export const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await destroyQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};