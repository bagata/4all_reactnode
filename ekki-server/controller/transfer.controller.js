/**
 * Transfer controller - Has all methods needed to create, read, update or delete a transfer in the database
 * Transfer stores user credit transfers to other users
 *
 * @summary Transfer CRUD
 * @author Lucas Bagatini
 */

import pool from '../middleware/db';
import * as accountController from './account.controller';
import * as statusHelper from '../helper/status.helper';

/**
 * CREATE Methods
 */

 const DIFF_MINUTES = 2;

// Saves a transference record
export const createQuery = async (transfer, connection) => connection.query('INSERT INTO transfers SET ?', [transfer]);
export const create = async (req, res) => {
    const conn = await pool.getConnection();
    await conn.beginTransaction();
    try {
        const { transfer } = req.body;
        if(!!transfer) {
            // Current transfer data
            const accountId = transfer.account_id;
            const amount = transfer.amount;
            const recipientId = transfer.recipient_id

            // Gets user account balance
            const [result] = await accountController.getBalanceQuery(accountId, conn);
            if(!!result && result.length > 0) {
                const balance = result[0].balance;
                const creditLimit = result[0].credit_limit;

                // Checks if user has enough balance (and credit limit) to make the transfer
                if(amount <= balance + creditLimit) {

                    // Checks if last transfer had the same amount, recipient and was done more than 2 minutes ago
                    const [lastTransfer] = await checkLastTransfer(accountId, recipientId, amount, conn);
                    if(!!lastTransfer && !!lastTransfer.length > 0 && lastTransfer[0].id) {
                        // Sets last transfer to CANCELED
                        await editQuery(lastTransfer[0].id, {"status": "CANCELED"}, conn);
                    }
                    else {
                        // Updates accounts balances by transfering money between them
                        await accountController.transferBalance(
                            accountId,
                            {"balance": (balance - amount)},
                            recipientId,
                            amount,
                            conn
                        );
                    }

                    // Registers new transference
                    await createQuery(transfer, conn);

                    // Commit changes
                    await conn.commit();

                    res.status(200).send({
                        success: true
                    });
                }
                else {
                    res.status(200).send({
                        success: false
                    });
                }
            }
        }
        else {
            // Rollback transference
            await conn.rollback();
            console.error('Rolling back')
            statusHelper.internalServerError(res);
        }
    }
    catch (error) {
        console.error(error);
        await conn.rollback();
        statusHelper.internalServerError(res);
    }
    finally {
        conn.release();
    }
};

/**
 * READ Methods
 */

// Gets all transfers (history)
export const listQuery = async () => pool.query('SELECT * FROM transfers');
export const list = async (req, res) => {
    try {
        const [result] = await listQuery();
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets transfer by id
export const getByIdQuery = async (id) => pool.query('SELECT * FROM transfers WHERE id = ?', [id]);
export const getById = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await getByIdQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets transfers by account id (user history)
export const getByAccountIdQuery = async (id) => {
    return pool.query(
        `SELECT trf.id, trf.amount, trf.status, DATE_FORMAT(trf.timestamp, '%d/%m/%Y %H:%i:%s') as timestamp, usr.name, usr.phone FROM transfers AS trf
        LEFT JOIN accounts AS acc
        ON trf.recipient_id = acc.id or trf.account_id = acc.id
        LEFT JOIN users AS usr
        ON acc.user_id = usr.id
        WHERE trf.account_id = ? or trf.recipient_id = ? and trf.status = 'SUCCESSFUL'
        GROUP BY trf.id
        `
        , 
        [id,id]
    )
};
export const getByAccountId = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await getByAccountIdQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets last transfer if had the same amount, recipient and was done more than 2 minutes ago
export const checkLastTransfer = async (account_id, recipient_id, amount, conn) => {
    return conn.query(
        `SELECT id FROM transfers WHERE account_id = ? AND recipient_id = ? AND amount = ? AND TIMESTAMPDIFF(MINUTE, timestamp, NOW()) < ${DIFF_MINUTES} ORDER BY ID DESC LIMIT 1`,
        [account_id, recipient_id, amount]
    );
}

/**
 * UPDATE Methods
 */
export const editQuery = async (id, transfer, conn) => conn.query('UPDATE transfers SET ? WHERE id = ?', [transfer, id]);
export const edit = async (req, res) => {
    const { id } = req.params;
    const { transfer } = req.body;
    try {
        const [result] = await editQuery(id, transfer, pool);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

 /**
 * DELETE Methods
 */
export const destroyQuery = async (id) => pool.query('DELETE FROM transfers WHERE id = ?', [id]);
export const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await destroyQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};