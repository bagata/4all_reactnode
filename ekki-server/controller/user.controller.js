/**
 * User controller - Has all methods needed to create, read, update or delete an user in the database
 *
 * @summary User CRUD
 * @author Lucas Bagatini
 */

import pool from '../middleware/db';

/**
 * CREATE Methods
 */

// Creates a new user
export const createQuery = async (user) => pool.query('INSERT INTO users SET ?', [user]);
export const create = async (req, res) => {
    const { user } = req.body;
    try {
        const [result] = await createQuery(user);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};


/**
 * READ Methods
 */

// Gets all users
export const listQuery = async () => pool.query('SELECT * FROM users');
export const list = async (req, res) => {
    try {
        const [result] = await listQuery();
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets user by id
export const getByIdQuery = async (id) => pool.query('SELECT * FROM users WHERE id = ?', [id]);
export const getById = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await getByIdQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

// Gets user by CPF number
export const getByCpfQuery = async (cpf) => { 
    return pool.query(
        `SELECT usr.*, acc.id as account_id, acc.balance, acc.credit_limit FROM users as usr 
        LEFT JOIN accounts as acc ON usr.id = acc.user_id 
        WHERE usr.cpf = ?`,
        [cpf]
    )
};
export const getByCpf = async (req, res) => {
    const { cpf } = req.params;
    try {
        const [result] = await getByCpfQuery(cpf);
        res.status(200).send({
            success: true,
            data: result[0]
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

/**
 * UPDATE Methods
 */
export const editQuery = async (id, user) => pool.query('UPDATE users SET ? WHERE id = ?', [user, id]);
export const edit = async (req, res) => {
    const { id } = req.params;
    const { user } = req.body;
    try {
        const [result] = await editQuery(id, user);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};

 /**
 * DELETE Methods
 */
export const destroyQuery = async (id) => pool.query('DELETE FROM users WHERE id = ?', [id]);
export const destroy = async (req, res) => {
    const { id } = req.params;
    try {
        const [result] = await destroyQuery(id);
        res.status(200).send({
            success: true,
            data: result
        });
    }
    catch (error) {
        console.error(error);
        statusHelper.internalServerError(res);
    }
};