require('dotenv').config();
import mysql from 'mysql2';

// Creates database connection
const pool = mysql.createPool({
    connectionLimit : process.env.DB_CONN_LIMIT,
    host            : process.env.DB_HOST,
    user            : process.env.DB_USER,
    password        : process.env.DB_PASS,
    database        : process.env.DB_NAME,
    port: 4000
});

const promisePool = pool.promise();

// Gets a connection instance from the pool
promisePool.getConnection((error, connection) => {
    // Handles connection errors
    if (!!error) {
        if (error.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Connection closed')
        }
        else if (error.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database connection limit reached')
        }
        else if (error.code === 'ECONNREFUSED') {
            console.error('Connection refused')
        }
        else {
            console.error('Something went wrong while trying to connect to the database');
        }
    }
    if (!!connection) {
        connection.release();
    }
    return;
})
export default promisePool;