/**
 * Has all routes access or modify a contact in the database
 *
 * @summary Contact routes
 * @author Lucas Bagatini
 */

import express from 'express';
import * as contactController from '../controller/contact.controller';
const router = express.Router();

/**
 * CREATE Routes
 */
router.post('/', contactController.create);

/**
 * READ Routes
 */

// All relations
router.get('/', contactController.list);

// Includes contact info (from user table)
router.get('/:id/info', contactController.getFullData);

// Gets by user id
router.get('/:id', contactController.getByUserId);

/**
 * UPDATE Routes
 */
router.put('/:id', contactController.edit);

/**
 * DELETE Routes
 */
router.delete('/:user_id/contact/:contact_id', contactController.destroy);

export default router;
