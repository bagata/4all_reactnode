/**
 * Has all routes to access or modify a transfer in the database
 *
 * @summary Transaction routes
 * @author Lucas Bagatini
 */

import express from 'express';
import * as transferController from '../controller/transfer.controller';
const router = express.Router();

/**
 * CREATE Routes
 */
router.post('/', transferController.create);

/**
 * READ Routes
 */

// All relations
router.get('/', transferController.list);

// By id
router.get('/:id', transferController.getById);

// By account id
router.get('/account/:id', transferController.getByAccountId);


/**
 * UPDATE Routes
 */
router.put('/:id', transferController.edit);

/**
 * DELETE Routes
 */
router.delete('/:id', transferController.destroy);

export default router;
