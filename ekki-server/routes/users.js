/**
 * Has all routes to access or modify an user in the database
 *
 * @summary User routes
 * @author Lucas Bagatini
 */

import express from 'express';
import * as userController from '../controller/user.controller';
const router = express.Router();

/**
 * CREATE Routes
 */
router.post('/', userController.create);

/**
 * READ Routes
 */

// All users
router.get('/', userController.list);

// By id
router.get('/:id', userController.getById);

// By cpf
router.get('/cpf/:cpf', userController.getByCpf);

/**
 * UPDATE Routes
 */
router.put('/:id', userController.edit);

/**
 * DELETE Routes
 */
router.delete('/:id', userController.destroy);

export default router;
