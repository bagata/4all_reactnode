/**
 * Has all routes to access or modify an account in the database
 *
 * @summary Account routes
 * @author Lucas Bagatini
 */

import express from 'express';
import * as accountController from '../controller/account.controller';
const router = express.Router();

/**
 * CREATE Routes
 */
router.post('/', accountController.create);

/**
 * READ Routes
 */

// All accounts
router.get('/', accountController.list);

// By id
router.get('/:id', accountController.getById);

/**
 * UPDATE Routes
 */
router.put('/:id', accountController.edit);

/**
 * DELETE Routes
 */
router.delete('/:id', accountController.destroy);

export default router;
