/**
 * Helps to format request status and json
 *
 * @summary Request status and json format
 * @author Lucas Bagatini
 */

 const INTERNAL_SERVER_ERROR = 500;
 const INTERNAL_SERVER_ERROR_MESSAGE = 'Internal server error';

// Interal Server error (500)
export const internalServerError = (res, errorCode) => {
    try {
        if(!!res) {
            res.status(INTERNAL_SERVER_ERROR).send({
                success: false,
                error: INTERNAL_SERVER_ERROR_MESSAGE
            });
        }
    }
    catch (error) {
        console.error('Unable to send internal server error status', error);
    }
};